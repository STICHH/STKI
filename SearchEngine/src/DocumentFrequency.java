/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.*;
/**
 *
 * @author REZA
 */

public class DocumentFrequency {
	
	private int df;
	
	public DocumentFrequency() {
		df = 0;
	}
	
	public DocumentFrequency(int df) {
		this.df = df;
	}
	
	public void updateFrequency() {
		df += 1;
	}
	
	public int getDocumentFrequency() {
		return df;
	}
	public String toString()
	{
		return Integer.toString(df);
	}

}

.I 11
.T
History of Computer 
.W
W
The stored-program electronic digital computer first appeared in the 1940s and, from a 
machine designed to perform calculations, quickly evolved into an information 
processing and communications machine that became indispensable to business, science 
and many individual people. This article investigates how changes and improvements in 
computer technology have led to the evolution of the electronic digital computer from a 
machine filling an entire room, costing several million Euros and able to perform only 
simple arithmetic operations to the powerful and versatile laptops, PCs and 
supercomputers we know today. 
 
Developments in four broad sets of technologies, often overlapping, paved the way for 
the development of todays computers, and contributed to what we now call Information 
and Communication Technologies ICT. These technologies are Technologies to aid 
Calculation, Technologies for Automation and Control, Technologies for Information 
Processing and Information Management, and Communication Technologies. Introduction 
To most people today, computers are just electronic devices that offer a means of 
accessing the Internet and World Wide Web, a way to read your e-mail, an aid to 
running a business and a way to facilitate writing using a word processor. To most of us 
computers do not normally conjure up the picture of a fast calculator, which was the 
purpose for which they were initially designed the first electronic computers were 
essentially large calculating machines. Even using the word computer to refer to an 
electronic machine is fairly new. Before World War II this term was generally applied 
as a job title to the human clerks often young women who performed routine 
computations for business, government and research purposes. These people were the 
computers of this period. 
 
The stored-program electronic digital computer first appeared in the 1940s and, from a 
machine designed to perform calculations, quickly evolved into an information 
processing and communications machine that became indispensable to business, science 
and many individual people. Advances in technology meant that computers became 
cheaper, smaller and much more capable. In the late 1960s the idea of a household 
having its own computer was unthinkable, but the advent of the Personal Computer 



UN
ES

CO
 – 

EO
LS

S

SA
MP

LE
 C

HA
PT

ER
S

COMPUTER SCIENCE AND ENGINEERING – History of Computer Hardware and Software Development – Arthur Tatnall 

©Encyclopedia of Life Support Systems EOLSS PC ten years later changed all this until today almost every business, and many homes, 
have their own PC. What is a Computer 
What we now call a computer can be more formally described as a Stored Program 
Electronic Digital Computer and in this article that is what I will use this term to mean. 
But apart from the human computers mentioned earlier, until the 1960s there were two 
different types of computer: Analogue Computers and Digital Computers. Whereas a 
digital computer stores its data and performs its operations using digital numeric 
representation usually using binary numbers, an analogue computer does this by 
analogy using quantities like electronic voltages, volumes of liquid or fractions of the 
turn of a wheel. This article is concerned primarily with digital computers as analogue 
computers have now all but disappeared from the scene, but we will give some 
consideration to the use of analogue computers before the 1940s. 
 
The dictionary defines a computer as an electronic device which is capable of 
receiving information data and performing a sequence of logical operations in 
accordance with a predetermined but variable set of procedural instructions program 
to produce a result in the form of information or signals. 
The key here is that a computer must be programmable and so able to perform various 
operations under the control of different programs – it must be able to do more than just 
a single thing. An electronic device that automatically performs one or more fixed set of 
operations – like that of a washing machine, an electric bread maker, an espresso coffee 
maker, an automatic car wash or a car engine management system cannot be called a 
computer. To be what we know as a general purpose computer the device must be 
capable of performing various different general sets of operations under the user s 
control it must be programmable, and the program must be able to be stored in the 
device hence the name  stored program computer. A stored-program digital computer 
stores its program (instructions) as well as its data in internal memory and typically 
makes use of what is known as von Neumann architecture. Conceptual Structure of a Computer 
 
Although they do not normally look quite like this, conceptually a computer can be 
considered to consist of the following components 
Figure Conceptual Structure of an Electronic Digital Computer 



UN
ES

CO
 – 

EO
LS

S

SA
MP

LE
 C

HA
PT

ER
S

COMPUTER SCIENCE AND ENGINEERING – History of Computer Hardware and Software Development – Arthur Tatnall 

Encyclopedia of Life Support Systems EOLSS 

In this article we will see how changes and improvements in each of these components 
has led to the evolution of the electronic digital computer from a machine filling an 
entire room, costing several million Euros and able to perform only simple arithmetic 
operations to the powerful and versatile PCs and supercomputers we know today. We 
will see how the evolution of these devices over the years has contributed greatly to the 
way that computer hardware, software and applications have developed. The input device offers a means of entering both data and instructions programs 

into a computer. Input devices over the years have included paper tape, punched 
cards, keyboards, mouse, touch screen and voice. The output device lets you see the results of whatever the computer has processed. 
Output devices have included paper tape, dot matrix printers, line printers, plotters, 
display screens, video projectors, laser printers, inkjet printers and sound. Control and processing devices sometimes called the Central Processing Unit or 
CPU use electronic components made up from values, transistors or integrated 
circuits and, as the name suggests, control and perform the actual processing or computing. Internal memory is used by the computer to temporarily store both data and 
programs during its operations. These memory devices 
have included magnetic cores, Williams tube CRT devices, mercury delay lines 
and integrated circuits. Storage devices allow the computer to retain large amounts of data for longer 
periods. They have included paper tape, magnetic tape, magnetic drums, magnetic 
hard disk, floppy disks, CDs, DVDs and memory sticks. As its name suggests, a digital computer operates on and stores its data in binary, or 
base-2 0-1, rather than decimal, or base-10 0-9 format.
.B
2004
.AArthur Tatnall 

©Encyclopedia of Life Support Systems.
.I 13
.TFootball, History and Memory 
The Heroes of Manchester United 
.W
In recent years historians and sociologists have broadened their analysis of sport to include
the study of sporting heroes, their changing styles and images and the meanings associated 
with them. This has been linked with the study of memory individual and collective, 
private and public and the way in which it provides a perspective of the past that is often 
quite different from mainstream, professional histories. This article attempts to combine
 these two approaches through a study of Manchester United Football Club and its leading 
players from the 1950s to the 1990s. It argues that the memories of these heroes, constructed
by the sporting public and the media, are fundamentally connected to each other and to the
 popular history of the club, particularly the Munich legend of 1958. The identities or images
 of footballers are thus not simply the result of their athletic talents or of the era in which they 
played but are also the product of memory and the manifold associations drawn between the
present and the past.

Introduction
Manchester United appears to be the epitome of the modern commercial 
market driven football club. Not only do its operating profits and turnover
easily outstrip any of its rivals in the English Premiership, it is in financial
 terms the biggest club in Europe, and probably the world. Yet any visitor to 
Manchester Uniteds old Trafford stadium can hardly fail to be impressed by
 the way in which the club exhibits its commercial dynamism alongside a
palpable sense of its history and tradition. The physical symbols of its glorious
and not so glorious past the statue of Sir Matt Busby, the Munich clock on the
main stand, the club museum have become fundamental elements of the
clubs self image. Even the entrance to the new megastore is adorned with
images of four of the clubs former players Duncan Edwards, star of the
renowned Busby Babes team of the 1950s Denis Law and George Best from
 the European Cup winning side of 1968 and Bryan Robson, captain of the
 team in the 1980s and early 1990s. Of course, this reflects a much broader link 
between heritage and commerce. Modern football clubs are only too aware that
their history sells it helps to define and differentiate their product in an
increasingly competitive market place. It also indicates, however, that a clubs 
identity is shaped as much by its own particular history as by the more general
 development of the game.

 This article explores the relationship between the particular and the
general through an analysis of the leading players the stars or heroes of 

24 



Football Studies, vol 3 no 2 2000
 
Manchester United Football Club from the late 1950s to the 1990s. While it
 takes into account the significant scholarship over the last decade concerned 
with the creation of sporting heroes, it also ties this in with the legend of 
Manchester United itself, rooted in the Munich plane crash of 1958 and the
 1968 European Cup triumph. From the 1960s Manchester United emerged as
the first English club whose public support extended beyond the region to the
nation, even establishing international loyalties. To what extent did its star
players reflect the clubs complex, ambiguous identity What similarities and
 differences can be seen in the popular perception of the foremost players of the
 1958-95 period Bobby Charlton, Denis Law, George Best and Eric Cantona 
These questions are addressed in the article. 

I argue that the images and meanings associated with leading Manchester 
United players both contemporaneous and historical by the public and the 
media were not unconnected but crucially related both to each other and to the
 history of the club. If we are to understand the nature of sporting heroism and 
its construction, it is worth looking at the significance of the heroes of the past
 alongside the sporting qualities of players and the broader role of local, 
regional, national, ethnic and racial identities. In this article I examine the
careers and representations of leading Manchester United players from the
Busby Babes era of the late 1950s through the Eric Cantona era of the mid-
1990s in order to examine the ways in which historical memories and identities 
are formulated and maintained over time.

 Sporting Heroes
 Recent research has focused closely on the figure of the sports hero. Much of
the impetus for this work came from the European University Institute in
 Florence whose seminars and conferences on the theme of the sporting hero in
contemporary Europe encouraged academics throughout Europe to consider
and examine the symbolic meanings attached to sports people in different
national cultural environments In British football, the period before the 1960s 
witnessed the primacy of the local hero. Studies of the Edwardian footballers 
Stephen Bloomer and Harold Fleming and of later hero Stanley Matthews have
examined the importance of these players to their respective communities. All
were essentially local lads, who were known affectionately as our Stephen,
our Harold and our Stan and succeeded in giving their provincial towns a
national prominence by virtue of their footballing skills. In the post World
War Two era, Newcastle Uniteds Jackie Milburn and Middlesbroughs Wilf
Mannion have been similarly perceived as organically linked to their local 
communities. Yet while all were undoubtedly heroes to the communities they
 represented, they were not all the same type of hero. The values prized in
sporting heroes differed across time and space. The prolific goal scorer or the
 skilful dribbling wizard were perhaps the arche types but heroes needed to be 
neither skilful nor particularly entertaining. In the industrial districts of

 25



 Football, History and Memory 

northern England and south Wales, those players who had strength, courage,
grit and determination were as likely to be embraced as those who mesmerised 
defenders and scored great goals. So the earliest football heroes were almost all local figures who managed
 to embody town, city or perhaps regional pride but whose celebrity rarely 
reached beyond the parochial to the national. Stanley Matthews, whose long 
career was capped by his famous performance in the Cup Final of 1953, was
 arguably the first truly national hero, eulogised as much in the pages of the The 
Times as in the Blackpool Gazette. But the nationally recognised footballer,
much less the national hero, was still a rare phenomenon by the early 1950s
 when the Scottish manager Matt Busby began to assemble his young
 Manchester United side 

A number of writers have located the turning-point of the status of
professional footballers in the early to mid-1960s, when the abolition of the
 maximum wage and the rise in salaries contributed to create a new type of
football star According to James Walvin, this period witnessed the

emergence of the modern player, acutely conscious of his earning
capacity, on and off the field, highly susceptible to lucrative and
tempting offers and increasingly committed to a style of life and 
behaviour which was in many crucial respects new.B1980s and early 1990s.A


Matthew Taylor
De Montfort University

Leicester, England